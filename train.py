# -*- coding: utf-8 -*-

# import comet before ML lib
from comet_ml import Experiment

# do other imports
import tensorflow as tf
from tensorflow.keras import layers
import numpy as np

from model import create_model
from comet_ml import ConfusionMatrix


# Add the following code anywhere in your machine learning file
experiment = Experiment(project_name="project 1", workspace="nollde")
experiment.set_name("experiment 002")

params = {
    "additional_parameter1": 3.0,
    "additional_parameter2": 14,
    "additional_parameter3": 15,
}
experiment.log_parameters(params)

(x_train, y_train), (x_val, y_val) = tf.keras.datasets.mnist.load_data()
x_train = x_train.reshape(60000, 784) / 255
x_val = x_val.reshape(10000, 784) / 255

model = create_model()
model.compile(
    optimizer=tf.keras.optimizers.RMSprop(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

experiment.log_text("Train harder!")
history = model.fit(
    x_train, y_train, batch_size=128, epochs=4, validation_data=(x_val, y_val)
)

experiment.log_metric("some_metric", 0.42)
experiment.log_image(x_val[0].reshape(28, 28))

cm = ConfusionMatrix()
y_pred = model.predict(x_val)
y_val_oh = np.eye(10)[y_val.astype(int)]
cm.compute_matrix(y_val_oh, y_pred)
experiment.log_confusion_matrix(matrix=cm)
