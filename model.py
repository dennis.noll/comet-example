# -*- coding: utf-8 -*-
import tensorflow as tf


def create_model():
    inputs = tf.keras.Input(shape=(784,), name="digits")
    x = tf.keras.layers.Dense(512, activation="relu", name="dense_1")(inputs)
    x = tf.keras.layers.Dense(512, activation="relu", name="dense_2")(x)
    outputs = tf.keras.layers.Dense(10, activation="softmax", name="predictions")(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model
